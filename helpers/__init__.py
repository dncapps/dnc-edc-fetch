"""
Helpers for schema files to use
"""

SCHEMA_TO_SOURCE_SYSTEM_TABLE = {
    'attendance': ['rtp', 'galaxy'],
    'events': ['appetize', 'bypass', 'guestreserve', 'quest'],
    'flights': ['oag'],
    'gamingjackpot': ['oasis'],
    'gamingoffer': ['oasis'],
    'gamingplayer': ['oasis'],
    'glsum': ['rtp', 'galaxy'],
    'itemsum': ['appetize', 'micros'],
    'labor': ['ksc_kronos', 'timemanager'],
    'menuitems': ['appetize', 'halo', 'micros_bi_api'],
    'reports': ['blackline_journal', 'inventory_audit', 'revenue_integration', 'order_to_cash', 'general_reporting'],
    'reservations': ['sabre'],
    'suite_events': ['guestreserve'],
    'transactions': ['appetize', 'bypass', 'halo', 'infogenesis', 'infogenesis_cloud', 'micros', 'micros_cloud',
                     'micros3700', 'netsuite', 'quest', 'rpro', 'titbit', 'zaui'],
    'weather': ['darksky'],
    'activity': ['EDC'],  # activity uses all_source_systems(), this is need to supply sources not covered in /
    # typical integrations
}


def all_source_systems():
    """Returns a unique list of all source systems across all schemas"""
    source_systems = set()
    for resource_source_systems in SCHEMA_TO_SOURCE_SYSTEM_TABLE.values():
        source_systems |= set(resource_source_systems)
    return list(source_systems)


def source_systems_for(schema):
    """Returns all source systems for a specific schema"""
    return SCHEMA_TO_SOURCE_SYSTEM_TABLE[schema]
